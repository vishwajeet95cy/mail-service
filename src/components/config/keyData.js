export const uniqueElements = (arr) => {
  var main = []

  var cc = arr.reduce((current, prev) => {
    var key = prev['_id']
    if (!current[key]) {
      current[key] = []
    } else {
      current[key].pop()
    }
    current[key].push(prev)
    return current
  }, {})

  Object.values(cc).map((cd) => {
    main.push(cd[0])
  })
  return main.sort(function (a, b) {
    var dateA = new Date(a.createdAt), dateB = new Date(b.createdAt)
    return dateB - dateA
  });
}

export const removeElements = (arr, id) => {

  var cc = arr.filter((item) => item._id !== id)

  return cc
}