import React from 'react'
import {
  LinearProgress
} from '@mui/material'
import { makeStyles } from '@mui/styles'

const useStyles = makeStyles((theme) => ({
  ss: {
    left: 0,
    right: 0,
    top: 0,
    zIndex: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    position: 'fixed'
  },
}))

const Loader = () => {

  console.log('Loader  render')
  const classes = useStyles()
  return (
    <div className={classes.ss}>
      <LinearProgress />
    </div>
  )
}

export default React.memo(Loader)
