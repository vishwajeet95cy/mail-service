import { ButtonBase, styled } from "@mui/material";


export const ImageButton = styled(ButtonBase)(({ theme }) => ({
  position: 'relative',
  color: '#C31818',
  lineHeight: 1.75,
  padding: '10px 6px',
  // verticalAlign: 'baseline',
  // [theme.breakpoints.down('sm')]: {
  //   width: '100% !important', 
  // },
  '&:hover, &.Mui-focusVisible': {
    zIndex: 1,
    color: '#0a58ca',
    '& .MuiImageBackdrop-root': {
      opacity: 0.15,
    },
    '& .MuiImageMarked-root': {
      opacity: 0,
    },
    '& .MuiTypography-root': {
      border: '4px solid currentColor',
    },
  },
}));